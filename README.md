**Appetiser Music Player App**

Architecture Pattern:  
 - MVC

Programming Language:  
 - Swift 

Libraries:  
 - Lottie  
 - RealmSwift  
 - Alamofire 5.4  
 - SDWebImage