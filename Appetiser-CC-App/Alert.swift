//
//  Alert.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import Foundation
import UIKit

class Alert {
    
    // Shows Alert View whenever called
    static func showAlertWith(title: String, message: String, from vc: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
}
