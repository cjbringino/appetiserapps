//
//  iTunesSong.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import Foundation
import RealmSwift

class RealmiTunesSong: Object {
    @objc dynamic var trackName = ""
    @objc dynamic var artworkUrl100 = ""
    @objc dynamic var artworkUrl60 = ""
    @objc dynamic var primaryGenreName = ""
    @objc dynamic var trackPrice = 0.0
    @objc dynamic var collectionPrice = 0.0
    @objc dynamic var wrapperType = ""
    @objc dynamic var artistName = ""
    @objc dynamic var collectionName = ""
    @objc dynamic var country = ""
    @objc dynamic var currency = ""
    @objc dynamic var previewUrl = ""
    @objc dynamic var releaseDate = ""
    @objc dynamic var kind = ""
    
    init(_ dic: NSDictionary) {
        trackName = dic["trackName"] as? String ?? ""
        artworkUrl100 = dic["artworkUrl100"] as? String ?? ""
        primaryGenreName = dic["primaryGenreName"] as? String ?? ""
        trackPrice = dic["trackPrice"] as? Double ?? 0.0
        collectionPrice = dic["collectionPrice"] as? Double ?? 0.0
        wrapperType = dic["wrapperType"] as? String ?? ""
        artistName = dic["artistName"] as? String ?? ""
        collectionName = dic["collectionName"] as? String ?? ""
        country = dic["country"] as? String ?? ""
        currency = dic["currency"] as? String ?? ""
        previewUrl = dic["previewUrl"] as? String ?? ""
        releaseDate = dic["releaseDate"] as? String ?? ""
        artworkUrl60 = dic["artworkUrl60"] as? String ?? ""
        kind = dic["kind"] as? String ?? ""
    }
    
    required override init() {
    }
}
