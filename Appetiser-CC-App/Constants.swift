//
//  Constants.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import Foundation

struct Constants {
    struct UserDefaults {
        static let kLastSongIndexPathRow = "kLastSongIndexPathRow"
        static let kLastVisitDate = "kLastVisitDate"
    }
}
