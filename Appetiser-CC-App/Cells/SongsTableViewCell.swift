//
//  SongsTableViewCell.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import UIKit
import SDWebImage

class SongsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTrackName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var ivArtwork: UIImageView!
    
    var iTunesSong: RealmiTunesSong? {
        didSet {
            displayData()
        }
    }
    
    private func displayData() {
        let song = iTunesSong!
        lblTrackName.text = song.trackName
        lblGenre.text = song.primaryGenreName
        lblPrice.text = "\(song.trackPrice)"
        lblCurrency.text = song.currency
        ivArtwork.sd_setImage(with: URL(string: song.artworkUrl60), placeholderImage: UIImage(named: "ic_music_gray_50px"))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
