//
//  SongsViewController.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import UIKit
import AVFoundation
import SDWebImage
import MediaPlayer

class SongDetailsViewController: BaseViewController {
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var lblTrackName: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblCurrentTime: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    
    @IBOutlet weak var btnPlay: UIButton!
    
    @IBOutlet weak var ivArtwork: UIImageView!
    @IBOutlet weak var pvDuration: UIProgressView!
    
    @IBOutlet weak var usVolume: UISlider!
    
    var iTunesSongs: [RealmiTunesSong]!
    var indexPathRow = 0
    
    private var volumeView: MPVolumeView!
    private let player = AVQueuePlayer()
    private let audioSession = AVAudioSession.sharedInstance()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        addRecognizer()
        addObserver()
        displayData()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func initView() {
        volumeView = MPVolumeView(frame: .zero)
        volumeView.isHidden = false
        volumeView.clipsToBounds = true
        volumeView.showsRouteButton = false
        volumeView.alpha = 0.0001
        vwMain.addSubview(volumeView)
    }
    
    private func addRecognizer() {
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe(_:)))
        swipeRecognizer.direction = .down
        vwMain.addGestureRecognizer(swipeRecognizer)
    }
    
    @objc
    private func didSwipe(_ recognizer: UISwipeGestureRecognizer) {
        switch recognizer.direction {
        case .down:
            dismiss()
        default:
            break
        }
    }
    
    private func addObserver() {
        listenVolumeButton()
        addAvPlayerObserver()
    }
    
    var outputVolumeObserve: NSKeyValueObservation?

    func listenVolumeButton() {
        do {
            try audioSession.setActive(true)
        } catch {}

        outputVolumeObserve = audioSession.observe(\.outputVolume) { (audioSession, changes) in
            self.usVolume.value = audioSession.outputVolume
        }
    }
    
    private func addAvPlayerObserver() {
        let interval = CMTime(value: 1, timescale: 2)
        player.addPeriodicTimeObserver(forInterval: interval, queue: .main) { progressTime in
            let seconds = CMTimeGetSeconds(progressTime)
            let formatter = NumberFormatter()
            formatter.minimumIntegerDigits = 2
            
            let secondsString = "\(formatter.string(from: NSNumber(value: seconds.truncatingRemainder(dividingBy: 60)))!)"
            let minutesString = "\(Int(seconds / 60))"
            let currentTime = "\(minutesString):\(secondsString)"
            
            self.lblCurrentTime.text = currentTime
            
            if let currentItem = self.player.currentItem {
                let durationSeconds = CMTimeGetSeconds(currentItem.duration)
                
                guard !durationSeconds.isNaN && !durationSeconds.isInfinite else {
                    return
                }
                
                let strDurationSeconds = "\(Int(durationSeconds.truncatingRemainder(dividingBy: 60)))"
                let strDurationMinutes = "\(Int(durationSeconds / 60))"
                let duration = "\(strDurationMinutes):\(strDurationSeconds)"
                
                let intSeconds = Int(seconds)
                let intDurationSeconds = Int(durationSeconds)
                
                let progressInt = Double(intSeconds) / Double(intDurationSeconds)
                
                self.pvDuration.progress = Float(progressInt)
                self.lblDuration.text = duration
                
                if intSeconds == intDurationSeconds {
                    self.player.seek(to: .zero)
                    self.pauseTheSong()
                }
            }
        }
    }
    
    private func displayData() {
        let vol = audioSession.outputVolume
        usVolume.value = vol
        
        let artworkUrl400 = iTunesSongs[indexPathRow].artworkUrl100.replacingOccurrences(of: "100x100", with: "400x400")
        lblAlbumName.text = iTunesSongs[indexPathRow].collectionName
        lblTrackName.text = iTunesSongs[indexPathRow].trackName
        lblArtistName.text = iTunesSongs[indexPathRow].artistName
        
        ivArtwork.sd_setImage(with: URL(string: artworkUrl400), placeholderImage: UIImage(named: "ic_music_gray_100px"))
        
        if let url = URL(string: iTunesSongs[indexPathRow].previewUrl) {
            player.removeAllItems()
            player.insert(AVPlayerItem(url: url), after: nil)
        }
        
        MPUserDefaults.saveIndexPathRow("\(indexPathRow)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        playTheSong()
    }
    
    @IBAction func didPlayDemo(_ sender: Any) {
        if player.isPlaying {
            pauseTheSong()
        } else {
            playTheSong()
        }
    }
    
    private func playTheSong() {
        btnPlay.setImage(UIImage(named: "img_pause_black_30px"), for: .normal)
        player.play()
    }
    
    private func pauseTheSong() {
        btnPlay.setImage(UIImage(named: "img_play_black_30px"), for: .normal)
        player.pause()
    }
    
    @IBAction func didPlayNextSong(_ sender: Any) {
        if indexPathRow != (iTunesSongs.count - 1) {
            playNextSong()
        }
    }
    
    private func playNextSong() {
        indexPathRow += 1
        pvDuration.progress = 0
        
        displayData()
        playTheSong()
    }
    
    @IBAction func didPlayPreviousSong(_ sender: Any) {
        if indexPathRow != 0 {
            if let currentItem = player.currentItem {
                if currentItem.currentTime().seconds < 2 {
                    playPreviousSong()
                } else {
                    player.seek(to: .zero)
                }
            }
        }
    }
    
    private func playPreviousSong() {
        indexPathRow -= 1
        pvDuration.progress = 0
        
        displayData()
        playTheSong()
    }
    
    @IBAction func didVolueValueChanged(_ sender: Any) {
        (volumeView.subviews.filter{NSStringFromClass($0.classForCoder) == "MPVolumeSlider"}.first as? UISlider)?.setValue(usVolume.value, animated: false)
    }
    
    @IBAction func didDismiss(_ sender: Any) {
        dismiss()
    }
    
    private func dismiss() {
        player.removeAllItems()
        dismiss(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        MPUserDefaults.removeIndexPathRow()
    }
}
