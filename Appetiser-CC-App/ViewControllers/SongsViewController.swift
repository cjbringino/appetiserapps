//
//  ViewController.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import UIKit

class SongsViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private lazy var service = iTunesSearchService()
    
    private var iTunesSongs = [RealmiTunesSong]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addListRefresher()
        fetchiTunesSongs(isRefresh: false)
    }
    
    // This function will let the user to pull refresh the list
    private func addListRefresher() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc
    private func didPullRefresh() {
        fetchiTunesSongs(isRefresh: true)
    }
    
    // Fetch the songs from the iTunes API
    private func fetchiTunesSongs(isRefresh: Bool) {
        showProgress()
        service.fetchRealmiTunesSongs(isRefresh: isRefresh) { iTunesSongs, strError, error in
            self.tableView.refreshControl?.endRefreshing()
            self.hideProgress()
            
            if let strError = strError {
                return Alert.showAlertWith(title: "Error", message: strError, from: self)
            } else if let error = error {
                return Alert.showAlertWith(title: "Error", message: error.localizedDescription, from: self)
            }
            
            self.appendDataAndReloadTable(iTunesSongs!)
        }
    }
    
    private func appendDataAndReloadTable(_ iTunesSongs: [RealmiTunesSong]) {
        let filterediTunesSongs = iTunesSongs.filter{( $0.wrapperType == "track" && $0.kind == "song" )}
        let sortediTunesSongs = filterediTunesSongs.sorted { song1, song2 in
            return song1.trackName < song2.trackName
        }
        self.iTunesSongs.removeAll()
        self.iTunesSongs.append(contentsOf: sortediTunesSongs)
        tableView.reloadData()
        
        checkForLastSongIndexPathRow()
    }
    
    // This function will check which track user was on
    private func checkForLastSongIndexPathRow() {
        let indexPathRow = MPUserDefaults.getLastIndexPathRow()
        
        if indexPathRow != nil {
            segueToSongDetails(Int(indexPathRow!)!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        displayLastVisitDate()
    }
    
    private func displayLastVisitDate() {
        if MPUserDefaults.getLastVisitDate() == nil {
            MPUserDefaults.saveLastVisitDate(Date())
        }
        
        guard let lastVisitDate = MPUserDefaults.getLastVisitDate() else { return }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        
        navigationItem.title = "Last Visit: \(dateFormatter.string(from: lastVisitDate))"
    }
}

extension SongsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return iTunesSongs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SongsTableViewCell", for: indexPath) as? SongsTableViewCell else { return UITableViewCell() }
        let iTunesSong = iTunesSongs[indexPath.row]
        
        cell.iTunesSong = iTunesSong
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        segueToSongDetails(indexPath.row)
    }
    
    // Segues to Song Details View
    private func segueToSongDetails(_ indexPathRow: Int) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "SongDetailsViewController") as! SongDetailsViewController
        vc.iTunesSongs = iTunesSongs
        vc.indexPathRow = indexPathRow
        
        present(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
