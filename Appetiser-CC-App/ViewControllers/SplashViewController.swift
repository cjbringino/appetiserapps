//
//  SplashViewController.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import UIKit
import Lottie

class SplashViewController: BaseViewController {
    @IBOutlet weak var vwLottie: UIView!
    
    // 1. Create the AnimationView
    private var animationView: AnimationView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // 2. Start AnimationView with animation name (without extension)
        
        animationView = .init(name: "music-play-lottie")
          
        animationView!.frame = vwLottie.bounds
        
        // 3. Set animation content mode
        
        animationView!.contentMode = .scaleAspectFit
        
        // 4. Set animation loop mode
        
        animationView!.loopMode = .loop
        
        // 5. Adjust animation speed
        
        animationView!.animationSpeed = 0.5
        
        vwLottie.addSubview(animationView!)
        
        // 6. Play animation
        
        animationView!.play()
        
        // 7. Segues to song list view after 1.5 seconds.
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { timer in
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "showSongList", sender: nil)
            }
        }
    }
}


//showSongList
