//
//  BaseViewController.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import UIKit

class BaseViewController: UIViewController {
    private var activityView: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showProgress() {
        let aiCGRect = CGRect(x: 0, y: 0, width: 80, height: 80)
        activityView = UIActivityIndicatorView(frame: aiCGRect)
        activityView?.center = self.view.center
        activityView?.color = .black
        activityView?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }
    
    func hideProgress() {
        activityView?.stopAnimating()
    }
}
