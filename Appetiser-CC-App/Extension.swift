//
//  Extension.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import Foundation
import AVFoundation
import UIKit

extension AVPlayer {
    
    // This property will check if the AVQueuePlayer is playing or paused.
    var isPlaying: Bool {
        if (self.rate != 0 && self.error == nil) {
            return true
        } else {
            return false
        }
    }
}

extension UIView {
    
    // This function can add border, border color, and corner radius to any Views that conforms with UIView
    func addCornerRadius(borderWidth: CGFloat, borderColor: UIColor, cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
}
