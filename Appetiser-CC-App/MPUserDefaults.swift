//
//  MPUserDefaults.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import Foundation

class MPUserDefaults {
    static func saveIndexPathRow(_ indexPathRow: String) {
        UserDefaults.standard.set(indexPathRow, forKey: Constants.UserDefaults.kLastSongIndexPathRow)
    }
    
    static func getLastIndexPathRow() -> String? {
        return UserDefaults.standard.string(forKey: Constants.UserDefaults.kLastSongIndexPathRow)
    }
    
    static func removeIndexPathRow() {
        UserDefaults.standard.set(nil, forKey: Constants.UserDefaults.kLastSongIndexPathRow)
    }
    
    static func saveLastVisitDate(_ date: Date) {
        UserDefaults.standard.set(date, forKey: Constants.UserDefaults.kLastVisitDate)
    }
    
    static func getLastVisitDate() -> Date? {
        return UserDefaults.standard.value(forKey: Constants.UserDefaults.kLastVisitDate) as? Date
    }
}
