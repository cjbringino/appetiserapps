//
//  iTunesSearchService.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import Foundation
import Alamofire
import RealmSwift

typealias FetchiTunesSongsResult = (_ itunesSongs: [RealmiTunesSong]?, _ strError: String?, _ error: Error?) -> Void

class iTunesSearchService {
    private lazy var realm = try! Realm()
    private let searchURL = "https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all"
    
    func fetchRealmiTunesSongs(isRefresh: Bool, completion: @escaping FetchiTunesSongsResult) {
        checkIfRealmiTunesSongsPersists(isRefresh, completion: completion)
    }

    // This function will check if a list of songs is persisted or not. If the list of songs is not yet persisted, it will pull the data from the API. Else, from local database
    private func checkIfRealmiTunesSongsPersists(_ isRefresh: Bool, completion: @escaping FetchiTunesSongsResult) {
        let realmiTunesSongs = getRealmiTunesSongs()
        
        if isRefresh {
            fetchiTunesSongsAndPersist(completion: completion)
        } else {
            if realmiTunesSongs.isEmpty {
                fetchiTunesSongsAndPersist(completion: completion)
            } else {
                completion(realmiTunesSongs, nil, nil)
            }
        }
    }

    // This function will fetch the list of songs from API and save it to local database.
    private func fetchiTunesSongsAndPersist(completion: @escaping FetchiTunesSongsResult) {
        AF.request(searchURL).responseJSON { response in
            guard response.error == nil else { return completion(nil, nil, response.error) }
            guard let dicResult = response.value as? NSDictionary
            else {
                return completion(nil, "Something went wrong... \nWrong cast. Please send this error to a developer. Thank you.", nil)
            }
            guard let results = dicResult["results"] as? NSArray
            else {
                return completion(nil, "Something went wrong... \nWrong cast. Please send this error to a developer. Thank you.", nil)
            }
            
            let iTunesSongs = results.map({ RealmiTunesSong($0 as! NSDictionary) })
            
            self.saveRealmiTunesSongs(iTunesSongs, completion: completion)
        }
    }
    
    private func saveRealmiTunesSongs(_ iTunesSongs: [RealmiTunesSong], completion: @escaping FetchiTunesSongsResult) {
        do {
            try realm.write {
                realm.deleteAll()
                realm.add(iTunesSongs)
            }
            
            completion(getRealmiTunesSongs(), nil, nil)
        } catch let error {
            completion(nil, nil, error)
        }
    }

    // Gets the list of songs from local database.
    private func getRealmiTunesSongs() -> [RealmiTunesSong] {
        return realm.objects(RealmiTunesSong.self).map({ $0 })
    }

    func deleteAllObjects() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch let error {
            print("Realm Error: \(error.localizedDescription)")
        }
    }
}
