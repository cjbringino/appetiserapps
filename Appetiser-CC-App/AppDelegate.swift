//
//  AppDelegate.swift
//  Appetiser-CC-App
//
//  Created by Christian Bringino on 8/23/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        let currentDate = Date()
        
        // Saves the device's current date when user suspends the application and it will be considered user's Last Visit
        MPUserDefaults.saveLastVisitDate(currentDate)
    }
}

